
from setuptools import setup, find_packages
from oopment.core.version import get_version

VERSION = get_version()

f = open('README.md', 'r')
LONG_DESCRIPTION = f.read()
f.close()

setup(
    name='oopment',
    version=VERSION,
    description='OoPment Developer tools',
    long_description=LONG_DESCRIPTION,
    long_description_content_type='text/markdown',
    author='Willie Slepecki',
    author_email='scphantm@gmail.com',
    url='https://gitlab.com/oopment/oopment',
    license='apache2',
    packages=find_packages(exclude=['ez_setup', 'tests*']),
    package_data={'oopment': ['templates/*']},
    include_package_data=True,
    entry_points="""
        [console_scripts]
        oopment = oopment.main:main
    """,
)
